#include<stdio.h>
#include <stdlib.h>
#include <omp.h>
int  ArrLen,threads;
void printList(int arr[], int len) {
	int i;
	for (i = 0; i < len; i++) {
		printf("%d\t", arr[i]);
	}
	printf("\n");
}
void merge(int arr[], int start, int mid, int end) {
	int result[ArrLen];
	int k = 0;
	int i = start;
	int j = mid + 1;
	while (i <= mid && j <= end) {
		if (arr[i] < arr[j]){
			result[k++] = arr[i++];
        }
		else{
			result[k++] = arr[j++];
        }
	}
	if (i == mid + 1) {
		while(j <= end)
			result[k++] = arr[j++];
	}
	if (j == end + 1) {
		while (i <= mid)
			result[k++] = arr[i++];
	}
	for (j = 0, i = start ; j < k; i++, j++) {
		arr[i] = result[j];
	}
}
 
void mergeSort(int arr[], int start, int end) {
	if (start >= end)
		return;
	int mid = ( start + end ) / 2;
	#pragma omp parallel sections
	{
	#pragma omp section
	mergeSort(arr, start, mid);
	#pragma omp section
	mergeSort(arr, mid + 1, end);
	#pragma omp section
	merge(arr, start, mid, end);
	}
}
 
int main(int argc,char **argv)
{
	
	printf("INPUT LENGTH:");
	scanf("%d",&ArrLen);
	printf("INPUT threads:");
	scanf("%d",&threads);
	int i,arr[ArrLen];
	for(i=0;i<ArrLen;i++){
		arr[i]=rand()%1000+1;
	}
	printf("original array\n");
	printList(arr, ArrLen);
	 omp_set_num_threads(threads);
	double t1=omp_get_wtime(); 
	mergeSort(arr, 0, ArrLen-1);
	double t2=omp_get_wtime();
	printf("sort array\n");
	printList(arr, ArrLen);
	printf("times: %f seconds\n",t2-t1);
	return 0;
}