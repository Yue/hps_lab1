#include<stdio.h>
#include <stdlib.h>
#include <omp.h>

int threads;
int min(int a,int b){
	if(a>b)
		return b;
	else 
		return a;
 return 0;
}

void insertionSort(int arr[], int left, int right) // Implementing insertionsort for RUN size chunks
{
   for (int i = left + 1; i <= right; i++){
      int t = arr[i];
      int j = i - 1;
      while (j >= left && t < arr[j]){
         arr[j+1] = arr[j--];
      }
      arr[j+1] = t;
   }
}
void merge(int arr[], int l, int m, int r) // using the merge function thesorted chunks of size 32 are merged into one
{
   int len1 = m - l + 1, len2 = r - m;
   int left[len1], right[len2];
   for (int i = 0; i < len1; i++)
      left[i] = arr[l + i]; // Filling left array
   for (int i = 0; i < len2; i++)
      right[i] = arr[m + 1 + i]; // Filling right array
   int i = 0;
   int j = 0;
   int k = l;
   while (i < len1 && j < len2) // Iterate into both arrays left and right
   {
      if (left[i] <= right[j]) // IF element in left is less then increment i by pushing into larger array
	  {
         arr[k] = left[i];
         i++;
      } else {
         arr[k] = right[j]; // Element in right array is greater increment j
         j++;
      }
      k++;
   }
   while (i < len1) // This loop copies remaining element in left array
   {
      arr[k] = left[i];
      k++;
      i++;
   }
   while (j < len2) // This loop copies remaining element in right array
   {
      arr[k] = right[j];
      k++;
      j++;
   }
}
void timSortAlgo(int arr[], int n){
	int s;
	omp_set_num_threads(threads);
#pragma omp parallel for 
   for (int i = 0; i < n; i+=32) 
	   insertionSort(arr, i, min((i+31), (n-1))); //Call insertionSort()
   for (s = 32; s < n; s = 2*s) // Start merging from size RUN (or 32). It will continue upto 2*RUN
   {
	  #pragma omp parallel for private(s)
      for (int left = 0;left < n;left += 2*s){
         int mid = left + s - 1; // 
         int right = min((left + 2*s - 1), (n-1));
         merge(arr, left, mid, right); 
      }
   }
}
void printArray(int arr[], int n){
   for (int i = 0; i < n; i++)
      printf("%d\t",arr[i]);
   printf("\n");
}
// Main function to implement timsort algorithm
int main(){
	int n;
	printf("INPUT LENGTH:");
	scanf("%d",&n);
	printf("INPUT threads:");
	scanf("%d",&threads);
   int arr[n];
   for(int i=0;i<n;i++){
		arr[i]=rand()%1000+1;
	}
   printf("The Original array\n");
   printArray(arr, n);
    
	double t1=omp_get_wtime();
   // calling the timsortAlgo function to sort array
   timSortAlgo(arr, n);
   double t2=omp_get_wtime();
   printf("After Sorting Array Using TimSort Algorithm\n");
   printArray(arr, n); // Calling print function
   printf("times: %f seconds\n",t2-t1);
   return 0;
}
